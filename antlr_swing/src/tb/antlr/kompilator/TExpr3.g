tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer licznik=0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text});
    
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dod(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odj(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st},p2={$e2.st}) 
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st}) 
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($ID.text)}? -> setVar(p1={$i1.text},p2={$e2.st})
        | ID                       {globals.hasSymbol($ID.text)}? -> getVar(id={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr) {licznik++;} -> if_statement(condition={$e1.st}, positive={$e2.st}, negative={$e3.st}, licznik={licznik.toString()})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
        
    ;
    